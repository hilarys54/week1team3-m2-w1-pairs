BEGIN TRANSACTION;

CREATE TABLE pet (
    pet_id serial NOT NULL,
    pet_name varchar(64),
    pet_type integer NOT NULL,
    pet_age integer,
    CONSTRAINT pk_pet_id PRIMARY KEY (pet_id)
);

CREATE TABLE animal_price (
    type_id integer NOT NULL,
    procedure_id integer NOT NULL, 
    cost real NOT NULL,
    CONSTRAINT pk_pet_price PRIMARY KEY (type_id, procedure_id)
);

CREATE TABLE type (
    type_id serial NOT NULL,
    type varchar(64),
    CONSTRAINT type_id PRIMARY KEY (type_id)
);

-- One owner will be the primary owner, but a pet can have two owners
CREATE TABLE pet_owner (
    pet_id serial NOT NULL,
    owner_id integer NOT NULL,
    second_owner_id integer,
    CONSTRAINT pk_pet_id_owner_id PRIMARY KEY (pet_id, owner_id)
);

CREATE TABLE owner (
    owner_id serial NOT NULL,
    first_name varchar(64) NOT NULL,
    last_name varchar(64) NOT NULL,
    honorific varchar(10),
    address varchar(64),
    city_id varchar(64) NOT NULL,
    CONSTRAINT pk_owner_id PRIMARY KEY (owner_id)
);

CREATE TABLE owner_invoice (
    owner_id integer NOT NULL,
    invoice_id integer NOT NULL,
    CONSTRAINT pk_owner_and_invoice_id PRIMARY KEY (owner_id, invoice_id)
);

CREATE TABLE city (
    city_id varchar(64) NOT NULL,
    name varchar(64) NOT NULL,
    district varchar(64),
    zip_code varchar(64),
    CONSTRAINT pk_city_id PRIMARY KEY (city_id)
);

CREATE TABLE invoice (
  invoice_id serial NOT NULL,
  total real NOT NULL,
  tax real NOT NULL,
  CONSTRAINT pk_invoice_id PRIMARY KEY (invoice_id)
);

CREATE TABLE visit (
    visit_id serial NOT NULL,
    visit_date date NOT NULL,
    procedure_id integer,  
    pet_id integer NOT NULL,
    CONSTRAINT pk_visit_id PRIMARY KEY (visit_id)
);

CREATE TABLE visit_procedure (
    visit_id integer NOT NULL,
    procedure_id integer NOT NULL,
    CONSTRAINT pk_visit_and_procedure PRIMARY KEY (visit_id, procedure_id)
);

CREATE TABLE procedure (
    procedure_id integer NOT NULL,
    name varchar(64) NOT NULL,
    CONSTRAINT pk_procedure_id PRIMARY KEY (procedure_id)
);

INSERT INTO type (type_id, type) VALUES (1, 'DOG');
INSERT INTO type (type_id, type) VALUES (2, 'CAT');
INSERT INTO type (type_id, type) VALUES (3, 'BIRD');

INSERT INTO pet (pet_id, pet_name, pet_type, pet_age) VALUES (246, 'ROVER', 1, 12);
INSERT INTO pet (pet_id, pet_name, pet_type, pet_age) VALUES (298, 'SPOT', 1, 2);
INSERT INTO pet (pet_id, pet_name, pet_type, pet_age) VALUES (341, 'MORRIS', 2, 4);
INSERT INTO pet (pet_id, pet_name, pet_type, pet_age) VALUES (519, 'TWEEDY', 3, 2);

INSERT INTO city (city_id, name, district, zip_code) VALUES (1, 'MY CITY', 'ONTARIO', 'Z5Z 6G6');
INSERT INTO city (city_id, name, district, zip_code) VALUES (2, 'MONTPELIER', 'VERMONT', '05062');

INSERT INTO owner (owner_id, first_name, last_name, honorific, address, city_id) VALUES (1, 'SAM', 'COOK', 'MR', '123 THIS STREET', 1);
INSERT INTO owner (owner_id, first_name, last_name, honorific, address, city_id) VALUES (2, 'TERRY', 'KIM', 'MX', '125 THIS STREET', 2);
INSERT INTO owner (owner_id, first_name, last_name, honorific, address, city_id) VALUES (3, 'RICHARD', 'COOK', 'MR', '123 THIS STREET', 1);

INSERT INTO pet_owner (pet_id, owner_id, second_owner_id) VALUES (246, 1, 3);
INSERT INTO pet_owner (pet_id, owner_id, second_owner_id) VALUES (341, 1, 3);
INSERT INTO pet_owner (pet_id, owner_id) VALUES (298, 2);
INSERT INTO pet_owner (pet_id, owner_id) VALUES (519, 2);

INSERT INTO procedure (procedure_id, name) VALUES (1, 'RABIES VACCINATION');
INSERT INTO procedure (procedure_id, name) VALUES (5, 'HEART WORM TEST');
INSERT INTO procedure (procedure_id, name) VALUES (8, 'TETANUS VACCINATION');
INSERT INTO procedure (procedure_id, name) VALUES (10, 'EXAMINE AND TREAT WOUND');
INSERT INTO procedure (procedure_id, name) VALUES (12, 'EYE WASH');
INSERT INTO procedure (procedure_id, name) VALUES (20, 'ANNUAL CHECKUP');

INSERT INTO visit (visit_id, visit_date, procedure_id, pet_id) VALUES (1, to_date('01132002', 'MMDDYYYY'), 1, 246);
INSERT INTO visit (visit_id, visit_date, procedure_id, pet_id) VALUES (2, to_date('03272002', 'MMDDYYYY'), 10, 246);
INSERT INTO visit (visit_id, visit_date, procedure_id, pet_id) VALUES (3, to_date('04022002', 'MMDDYYYY'), 5, 246);
INSERT INTO visit (visit_id, visit_date, procedure_id, pet_id) VALUES (4, to_date('01212002', 'MMDDYYYY'), 8, 298);
INSERT INTO visit (visit_id, visit_date, procedure_id, pet_id) VALUES (5, to_date('03102002', 'MMDDYYYY'), 5, 298);
INSERT INTO visit (visit_id, visit_date, procedure_id, pet_id) VALUES (6, to_date('01232001', 'MMDDYYYY'), 1, 341);
INSERT INTO visit (visit_id, visit_date, procedure_id, pet_id) VALUES (7, to_date('01132002', 'MMDDYYYY'), 1, 341);
INSERT INTO visit (visit_id, visit_date, procedure_id, pet_id) VALUES (8, to_date('04302002', 'MMDDYYYY'), 20, 519);
INSERT INTO visit (visit_id, visit_date, procedure_id, pet_id) VALUES (9, to_date('04302002', 'MMDDYYYY'), 12, 519);

INSERT INTO visit_procedure (visit_id, procedure_id) VALUES (1, 1);
INSERT INTO visit_procedure (visit_id, procedure_id) VALUES (2, 10);
INSERT INTO visit_procedure (visit_id, procedure_id) VALUES (3, 5);
INSERT INTO visit_procedure (visit_id, procedure_id) VALUES (4, 8);
INSERT INTO visit_procedure (visit_id, procedure_id) VALUES (5, 5);
INSERT INTO visit_procedure (visit_id, procedure_id) VALUES (6, 1);
INSERT INTO visit_procedure (visit_id, procedure_id) VALUES (7, 1);
INSERT INTO visit_procedure (visit_id, procedure_id) VALUES (8, 20);
INSERT INTO visit_procedure (visit_id, procedure_id) VALUES (9, 12);

INSERT INTO animal_price (type_id, procedure_id, cost) VALUES (1, 1, 30);
INSERT INTO animal_price (type_id, procedure_id, cost) VALUES (2, 1, 24);

INSERT INTO invoice (invoice_id, tax, total) VALUES (1, .08, 58.32);

INSERT INTO owner_invoice (owner_id, invoice_id) VALUES (1, 1);

ALTER TABLE pet_owner 
ADD FOREIGN KEY (owner_id)
REFERENCES owner(owner_id);

ALTER TABLE pet_owner 
ADD FOREIGN KEY (pet_id)
REFERENCES pet(pet_id);

ALTER TABLE visit
ADD FOREIGN KEY (procedure_id)
REFERENCES procedure(procedure_id);

ALTER TABLE visit_procedure
ADD FOREIGN KEY (procedure_id)
REFERENCES procedure(procedure_id);

ALTER TABLE visit_procedure
ADD FOREIGN KEY (visit_id)
REFERENCES visit(visit_id);

ALTER TABLE animal_price
ADD FOREIGN KEY (type_id)
REFERENCES type(type_id);

ALTER TABLE owner
ADD FOREIGN KEY (city_id)
REFERENCES city(city_id);

ALTER TABLE animal_price
ADD FOREIGN KEY (procedure_id)
REFERENCES procedure(procedure_id);

ALTER TABLE invoice
ADD FOREIGN KEY (invoice_id)
REFERENCES invoice(invoice_id);

ALTER TABLE owner_invoice
ADD FOREIGN KEY (owner_id)
REFERENCES owner(owner_id);

ALTER TABLE owner_invoice
ADD FOREIGN KEY (invoice_id)
REFERENCES invoice(invoice_id);

ALTER TABLE pet
ADD FOREIGN KEY (pet_type)
REFERENCES type(type_id);

END;