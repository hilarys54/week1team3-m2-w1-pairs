-- Write queries to return the following:
-- The following queries utilize the "world" database.

-- 1. The city name, country name, and city population of all cities in Europe with population greater than 1 million
--    Expected rows: 36

SELECT city.name, country.name, city.population
FROM city
INNER JOIN country ON city.countrycode = country.code
WHERE country.continent = 'Europe' AND city.population > 1000000;

-- 2. The city name, country name, and city population of all cities in countries where French is an official language and the city population is greater than 1 million
--    Expected rows: 2

SELECT city.name, country.name, city.population
FROM city
INNER JOIN country ON city.countrycode = country.code
INNER JOIN countrylanguage ON countrylanguage.countrycode = city.countrycode
WHERE countrylanguage.language = 'French' AND countrylanguage.isofficial = true AND city.population > 1000000;

-- 3. The name of the countries and continents where the language Javanese is spoken
--    Expected rows: 1

SELECT country.name, country.continent
FROM country
INNER JOIN countrylanguage ON country.code = countrylanguage.countrycode
WHERE countrylanguage.language = 'Javanese'
GROUP BY country.name, country.continent;

-- 4. The names of all of the countries in Africa that speak French as an official language
--    Expected rows: 5

SELECT country.name
FROM country 
INNER JOIN countrylanguage ON country.code = countrylanguage.countrycode
WHERE countrylanguage.language = 'French' AND isofficial = true AND country.continent = 'Africa';

-- 5. The average city population of cities in Europe
--    Expected: 287,684

SELECT ROUND(AVG(city.population))
FROM city 
INNER JOIN country ON city.countrycode = country.code
WHERE continent = 'Europe';

-- 6. The average city population of cities in Asia
--    Expected: 395,019

SELECT ROUND(AVG(city.population))
FROM city 
INNER JOIN country ON city.countrycode = country.code
WHERE continent = 'Asia';

-- 7. The number of cities in countries where English is an official language
--    Expected: 523

SELECT COUNT(city.name)
FROM city
INNER JOIN countrylanguage ON city.countrycode = countrylanguage.countrycode
WHERE language = 'English' AND isofficial = true;

-- 8. The average population of cities in countries where the official language is English
--    Expected: 285,809

SELECT ROUND(AVG(city.population))
FROM city
INNER JOIN countrylanguage ON city.countrycode = countrylanguage.countrycode
WHERE language = 'English' AND isofficial = true;

-- 9. The names of all of the continents and the population of the continent’s largest city
--    Expected rows: 6, largest population for North America: 8,591,309

SELECT country.continent, MAX(city.population)
FROM country
INNER JOIN city ON country.code = city.countrycode
GROUP BY continent;

-- 10. The names of all of the cities in South America that have a population of more than 1 million people and the official language of each city’s country
--     Expected rows: 29

SELECT city.name, city.population, countrylanguage.language
FROM city
INNER JOIN countrylanguage ON city.countrycode = countrylanguage.countrycode
INNER JOIN country ON city.countrycode = country.code
WHERE city.population > 1000000 AND countrylanguage.isofficial = true AND country.continent = 'South America';
                                    
