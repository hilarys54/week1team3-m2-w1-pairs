-- Write queries to return the following:
-- Make the following changes in the "world" database.

-- 1. Add Superman's hometown, Smallville, Kansas to the city table. The 
-- countrycode is 'USA', and population of 45001. (Yes, I looked it up on 
-- Wikipedia.)

INSERT INTO city (id, name, district, countrycode, population) VALUES (4080, 'Smallville', 'Kansas', 'USA', 45001);

-- 2. Add Kryptonese to the countrylanguage table. Kryptonese is spoken by 0.0001
-- percentage of the 'USA' population.

INSERT INTO countrylanguage (countrycode, language, isofficial, percentage) VALUES ('USA', 'Kryptonese', false, .0001);

-- 3. After heated debate, "Kryptonese" was renamed to "Krypto-babble", change 
-- the appropriate record accordingly.

UPDATE countrylanguage SET language = 'Krypto-babble' WHERE language = 'Kryptonese';

-- 4. Set the US captial to Smallville, Kansas in the country table.

UPDATE country SET capital = (SELECT id FROM city WHERE name = 'Smallville');

-- 5. Delete Smallville, Kansas from the city table. (Did it succeed? Why?)

DELETE FROM city WHERE name = 'Smallville';
-- No, smallville is still referenced on the country table

-- 6. Return the US captial to Washington.

UPDATE country SET capital = (SELECT id FROM city WHERE name = 'Washington DC');

-- 7. Delete Smallville, Kansas from the city table. (Did it succeed? Why?)

DELETE FROM city WHERE name = 'Smallville';
-- Yes, not referenced from country table any more

-- 8. Reverse the "is the official language" setting for all languages where the
-- country's year of independence is within the range of 1800 and 1972 
-- (exclusive). 
-- (590 rows affected)
    
UPDATE countrylanguage 
SET isofficial = NOT isofficial
WHERE countrylanguage.countrycode IN 
(SELECT code
FROM country
WHERE country.indepyear > 1800 AND country.indepyear < 1972);

-- 9. Convert population so it is expressed in 1,000s for all cities. (Round to
-- the nearest integer value greater than 0.)
-- (4068 rows affected)

UPDATE city SET population = ROUND(population / 1000);

-- 10. Assuming a country's surfacearea is expressed in miles, convert it to 
-- meters for all countries where French is spoken by more than 20% of the 
-- population.
-- (7 rows affected)

UPDATE country 
SET surfacearea = surfacearea * 2590000
WHERE surfacearea IN (
SELECT surfacearea 
FROM country
INNER JOIN countrylanguage ON country.code = countrylanguage.countrycode 
WHERE language = 'French' AND percentage > 20);

